namespace :dbimport do
  desc "Import schools"
  
  
  task :schoolimport  => :environment do
    skola = "http://www3.skolverket.se/ki03/lista_skoladress.aspx?skolform=11&huvudmannatyp=-&lan=-&kommun=-&skola_namn=&geoskolform=-&fran_ki=J&include=J&ar=1011&geolan=-&sprak=%24SPRAK"
    skola = "http://bit.ly/lNH8Jv"
    
    doc = Nokogiri::HTML(open(skola))
    trs = ''
    
    schools = Array.new
    
    trs = doc.css("tr.svanl, tr.pjamas")
    logger.info trs.class()
    
    trs.each do |trmatch|

        school = Array.new
        td = trmatch.css("td")  
        
        i = 0
        td.each do |tdmatch|
          if i == 0 
            if(tdmatch.css("a").length != 0)
              #logger.info tdmatch.css("a").first.content
              school<<tdmatch.css("a").first.content
              school<<tdmatch.css("a").first['href']
              #logger.info tdmatch.css("a").first['href']
            else
              #logger.info "no url!"
              #logger.info  tdmatch.content
              school<<tdmatch.content
              school<<nil
            end
          elsif i == 2
            info = tdmatch.content.split("  ")
            school<<info[0]
            school<<info[1]
          else
            school<<tdmatch.content
          end
          i = i+1
        end #end tds
        
      schools<<school
      
    end #end trs

    logger.info "ARRAY BUILD DONE"
    logger.info schools.length
    logger.info schools.first.length
    
    test = schools.first
    logger.info test[0] #namn
    logger.info test[1] #url
    logger.info test[2] #adress
    logger.info test[3] #postnmr
    logger.info test[4] #stad
    logger.info test[5] #fone
    logger.info test[6] #län
    logger.info test[7] #kommun
    logger.info test[8] #typ
    
    schools.each do |s|
      
      schoolGet = School.where(:name => s[0], :address => s[2], :postalcode => s[3])
      
      if schoolGet.length == 0
      
        muniAct = Municipality.where(:name => s[7])
        
        if muniAct.length == 0
          muni = Municipality.new
          muni.name = s[7]
          muni.save
          muniInput = muni
        else
          muniInput = muniAct.first
        end
        
        school = School.new do |sch|
          sch.name = s[0]
          sch.address = s[2]
          sch.postalcode = s[3]
          sch.city = s[4]
          sch.phone = s[5]
          sch.schooltype = "Grundskola"
          sch.county = s[6]
          sch.organization_type = s[8]
          sch.municipality = muniInput
          sch.url = s[1] 
        end
  
        school.save
      end
    end
  end  
  
  task :personalinfoimport  => :environment do
    doc = Nokogiri::HTML(File.open("tasks/personalinfo.xls"))
    trs = ''
    
    schools = Array.new
    
    trs = doc.css("tr")
    logger.info trs.class()
    
    counter = 0
    doImport = false
    
    schools = Array.new
    
    trs.each do |trmatch|
      if trmatch.content == 'Kommunala skolor' && counter < 100
        logger.info trmatch.content
        doImport = true
      end
      
      if doImport
        cells = trmatch.css('td')
        
        if !(cells.length == 0 || cells.length == 1) then
          
          if(cells.first.content == 'Kommunala skolor')
            break
          end
          
          tmpschools = School.includes("municipality").where(:name => cells[1].content, :municipalities => {:name => cells.first.content})
          if(tmpschools.length == 1)
          
            counter+=1
            
            school = tmpschools.first
            
            school.qualified_teachers = getvaluefromcell(cells[6])
            school.qualified_special_teachers = getvaluefromcell(cells[7])
            school.teacher_concentration = getvaluefromcell(cells[8])
            school.number_of_students = getvaluefromcell(cells[9])                                    
            #school.save
            #6 : behöriga lörare
            #7 : behöriga specialpedagoger
            #8 : lärare per 100 elever
            #9 : antal elever          
            
            schools << school.qualified_teachers
          
          #elsif(cells[1].content != 'Kommunplacerade grundsk.lärare')          
          end#skola finns i databas
        
        end#cell length if
        
      end #import if
      
    end #row loop
    
    logger.info "räknare: " + counter.to_s
    
    schools.each do |school|
      if(school < 0)
        logger.info school
      end
    end
    
  end #personal info task 
  
  def getvaluefromcell(cell)
    if cell.content == '.'
      return -1.0
    end
    if cell.content == '..'
      return -2.0
    end
    
    return cell.content.gsub(',', '.').to_f
  end
    
  task :res9import  => :environment do
    
    doc = Nokogiri::HTML(File.open("tasks/resnio.html"))
    trs = ''
        
    trs = doc.css("tr")
    logger.info trs.class()
    
    counterAdded = 0
    counterFound = 0
    
    doImport = false
    
    schools = Array.new
    
    trs.each do |trmatch|
      
      if trmatch.content == 'Kommunala skolor' && counterFound < 100
        logger.info trmatch.content
        doImport = true
      end
      
      if doImport
        cells = trmatch.css('td')
        
        if !(cells.length == 0 || cells.length == 1) then
          
          counterFound+=1
          
          if(cells.first.content == 'Kommunala skolor i urvalet')
            break
          end
          
          tmpschools = School.includes("municipality").where(:name => cells[0].content, :municipalities => {:name => cells[1].content})
          if(tmpschools.length == 1)
          
            counterAdded+=1
            
            school = tmpschools.first
            
            #3 : genomsnittligt meritvärde
            
            school.results_ninth_grade = getvaluefromcell(cells[3])                
            school.save
            
            schools << school.name
            logger.info cells[3].content
          
          end #skola finns i databas
        
        end #cell length if
        
      end #import if
      
    end #row loop
    
    
    schools.each do |school|
      logger.info school
    end
    
    logger.info "räknare added: " + counterAdded.to_s
    logger.info "räknare found: " + counterFound.to_s
    
  end #res9 info task
  
end