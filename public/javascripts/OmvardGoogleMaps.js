﻿function createMarker(point, tabs, icon_path) {
    var numberedIcon = new GIcon(G_DEFAULT_ICON);
    numberedIcon.image = icon_path;
    numberedIcon.iconSize = new GSize(27, 27);
    numberedIcon.shadowSize = new GSize(35, 27);
    numberedIcon.iconAnchor = new GPoint(27, 27);
    numberedIcon.infoWindowAnchor = new GPoint(13, 0);
    numberedIcon.shadow = "/Content/Images/GoogleMapIcons/shadow.png";
    markerOptions = { icon: numberedIcon };
    var marker = new GMarker(point, markerOptions);
    //var marker = new GMarker(point);

    GEvent.addListener(marker, "click", function() {
        marker.openInfoWindowTabsHtml(tabs);
    });

    return marker;
}

var markers = new Array();

/*
function openInfoWindow(index, tab) {
markers[index].openInfoWindowTabsHtml(info[index], { selectedTab: tab });
}

function closeInfoWindow(index) {
markers[index].closeInfoWindow();
}
*/

var map;

var latlngbounds;

function InitializeCountyPage() {
      if (GBrowserIsCompatible()) {
          map = InitMap(new GLargeMapControl3D(), new GSize(901, 420));
          var latlngbounds = InitLngBounds();          
          InitCountyMarkers();
          //window.setTimeout(InitCountyMarkers, 0);       

          if (latlng.length > 0)
              map.setCenter(latlngbounds.getCenter(), map.getBoundsZoomLevel(latlngbounds));
      }
  }

  function InitCountyMarkers() {
      var numMarkers = latlng.length;
      
      var minZoomBigMarkers = parseInt(numMarkers / 9);
      if (minZoomBigMarkers > 13)
          minZoomBigMarkers = 13;
      else if(minZoomBigMarkers < 7)
          minZoomBigMarkers = 7;

      mgr = new MarkerManager(map);
      mgr.addMarkers(CreateCountyMarkers(1), minZoomBigMarkers);
      mgr.addMarkers(CreateCountyMarkers(2), 0, minZoomBigMarkers-1);
      mgr.refresh();
  }

var basicIcon = "/Content/Images/GoogleMapIcons/basic.png";
var basicIconSmall = "/Content/Images/GoogleMapIcons/basicSmall.png";

  function CreateCountyMarkers(size) {
      var batch = [];
      
      for (var i = 0; i < latlng.length; i++) {
          var iconImg = new GIcon(G_DEFAULT_ICON);
          if (size == 1) {
              iconImg.image = basicIcon;
              iconImg.iconSize = new GSize(27, 27);
              iconImg.shadowSize = new GSize(35, 27);
              iconImg.iconAnchor = new GPoint(27, 27);
              iconImg.infoWindowAnchor = new GPoint(13, 0);
              iconImg.shadow = "/Content/Images/GoogleMapIcons/shadow.png";

          }
          else if (size == 2) {
              iconImg.image = basicIconSmall;
              iconImg.iconSize = new GSize(10, 10);
              iconImg.shadowSize = null;
              iconImg.iconAnchor = new GPoint(10,10);
              iconImg.infoWindowAnchor = new GPoint(3, 0);
              iconImg.shadow = null;
          }
          markerOptions = { icon: iconImg };

          var marker = CreateCountyMarker(latlng[i], info[i], markerOptions);
          
          batch.push(marker);  
      }
      return batch;
  }

  function CreateCountyMarker(point, tabs, markerOpts) {
      var marker = (new GMarker(point, markerOpts));

      GEvent.addListener(marker, "click", function() {
          marker.openInfoWindowTabsHtml(tabs);
      });
      
      return marker;
  }


function InitializeSearchPage() {
    if (GBrowserIsCompatible()) {
        map = InitMap(new GLargeMapControl3D());

        InitMarkers();

        var latlngbounds = InitLngBounds();

        if (latlng.length > 0)
            map.setCenter(latlngbounds.getCenter(), map.getBoundsZoomLevel(latlngbounds));
    }
}

function InitializeHOPage(id, height) {
    if (GBrowserIsCompatible()) {
        map = InitMap2(null, new GSize(169, height), "map_canvas");

        map.disableInfoWindow();
        map.disableDragging();
        map.disableDoubleClickZoom();
        map.disableScrollWheelZoom();
        map.disablePinchToZoom();

        GEvent.addListener(map, "click", function() {
            $.facebox({ ajax: '/vardgivare/map/' + id });
        });

        InitMarkers();

        var latlngbounds = InitLngBounds();

        if (latlng.length > 0)
            map.setCenter(latlngbounds.getCenter(), 14);

        return true;
    }
}

function InitializeBox() {
    if (GBrowserIsCompatible()) {
        map = InitMap2(new GLargeMapControl3D(), new GSize(600, 400), "map_canvas_box");

        map.disableInfoWindow();      
        
        InitMarkers();

        var latlngbounds = InitLngBounds();

        if (latlng.length > 0)
            map.setCenter(latlngbounds.getCenter(), 14);
        
        return true;
    }
}

function InitMap(control, mapsize) {
    return InitMap2(control, mapsize, "map_canvas");
}

function InitMap2(control, mapsize, aDiv) {
    if(mapsize != undefined)
		map = new google.maps.Map(document.getElementById(aDiv), { size: mapsize });
    else
        map = new google.maps.Map(document.getElementById(aDiv), { size: mapsize });

    map.setCenter(new GLatLng(61.6965396, 14.8684927), 4);
    
    if(control)
        map.addControl(control);
        
    return map;
}

function InitMarkers() {
    var path = "/Content/Images/GoogleMapIcons/";

    for (var i = 0; i < latlng.length; i++) {
        markers[i] = createMarker(latlng[i], info[i], path + (i + 1) + ".png");
        map.addOverlay(markers[i]);
        //map.addOverlay(createMarker(latlng[i], info[i], path));
    }
}

function InitLngBounds(){
        var latlngbounds = new GLatLngBounds();
        for (var i = 0; i < latlng.length; i++) {
            latlngbounds.extend(latlng[i]);
        }
        return latlngbounds;
    }