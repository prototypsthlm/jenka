// Scripts from Vardcentralsguiden
function doSubmit() {
	try {
        mapError = null;
        findAddress($('#guide-map-search-address').val() + ', Sverige', false);
        DoValidatation();
    } catch (e) {
    }
}

function DoValidatation() {
    if (mapError == null) {
        setTimeout(function () {
            DoValidatation();
        }, 200);   
    }
    else {
        if (($('#answer1').val() == "0" && $('#answer2').val() == "0" && $('#answer3').val() == "0") || (mapError == true || $('#answer5').val() == "")) {
            if (mapError == true || $('#answer5').val() == "") {
                $('#guide-map-popup').fadeIn();
                $.scrollTo('#guide-map-popup', 800);
                
                firstLoad = true;
                $.address.value('');
                
                return false;
            }
            if ($('#answer1').val() == "0" && $('#answer2').val() == "0" && $('#answer3').val() == "0") {
                $('#guide-group-popup-2').fadeIn();
                $.scrollTo('#guide-group-popup-2', 800);
                
                firstLoad = true;
                $.address.value('');
                
                return false;
            }
        }
        else
            DoResultPost();
    }                
}

function DoResultPost() {
    $('#loader').show();
    $('#guide-question-collection').fadeTo(400, 0.3);
    jQuery.ajax({
        type: 'POST',
        data: $('#guideForm').serialize(),
        url: '/school/search',
        complete: function (result, status) {
            $('#loader').hide();
        },
        success: function (result, status) {
            $('#guideresult').html(result);
        }
    });
}

function getNumberOfHits() {
    jQuery.ajax({
        type: 'POST',
        data: $('#guideForm').serialize(),
        url: '/school/getNumberOfHits',
        success: function (result, status) {
            $('#numberOfHits').html(result);
        }
    });
}

function loadForm() {
    try {
        $('#loader').show();
        $('#guide-question-collection').fadeTo(400, 0.3);
        jQuery.ajax({
            type: 'POST',
            url: '/Guide/VardcentralsguidenFormular',
            complete: function (result, status) {
                $('#loader').hide();
            },
            success: function (result, status) {
                $('#guide-page').html(result);
                $.scrollTo('#page', 800);
                initialize();
                setUpAddressChange();
            },
            error: function () { alert('Hej'); }
        });
    } catch (e) {
        alert('Hej2');
    }
}

var map = null;
var geocoder = null;
var marker = null;

function mapSearchClick() {
    findAddress($('#guide-map-search-address').val(), true);
}

function mapSearchKeyPress(e) {
    var eve = e || window.event;
    var keycode = eve.keyCode || eve.which;

      if (keycode == 13) {
        eve.cancelBubble = true;
        eve.returnValue = false;

        if (eve.stopPropagation) {   
          eve.stopPropagation();
          eve.preventDefault();
        }
        findAddress($('#guide-map-search-address').val(), true);
      }
      return false;
}

function initialize() {
        //map = InitMap2(null, new GSize(450, 300), "guide_map_canvas_box");
        geocoder = new google.maps.Geocoder();
		var latlng = new google.maps.LatLng(61.6965396, 14.8684927);
		var myOptions = {
	      zoom: 4,
	      center: latlng,
	      mapTypeId: google.maps.MapTypeId.ROADMAP
	    };
	    map = new google.maps.Map(document.getElementById("guide_map_canvas_box"), myOptions);
}

function findAddress(address, show) {
    var a = address;
    if (a.toLowerCase().indexOf('sverige') == -1)
        a += ', Sverige';

    if (geocoder)
        geocoder.geocode( { 'address': a }, addAddressToMap);
    else {
        initialize();
        if (geocoder)
            geocoder.geocode( { 'address': a }, addAddressToMap);
    }

    $('#answer7').val(a);
	
    if(show == true)
        if ($('#map-question-container-bottom').height() < 300) 
            $('#map-question-container-bottom').animate({ "height": 374 }, { duration: "slow" });
}

function logMapSearch(searchString, status) {
    try {
        jQuery.ajax({
            type: 'POST',
            url: '/Guide/LogGuideMapSearch',
            data: 'searchString=' + searchString + '&status=' + status
        });
    } catch (e) { }
}

// function for finding specific values in JSON object
function eachRec(obj) {
    $.each(obj, function (key, value) {
        if (key == 'PostalCodeNumber') {
            postalCode = value;
            return
        }
        if (key == 'LocalityName') {
            city = value.split(' ')[0];
            return
        }
        if (key != null && typeof value == 'object') eachRec(value);
    });
}

var postalCode = null;
var city = null;
var mapError = null;        

// Deletes all markers in the array by removing references to them
function deleteOverlays() {
  if (markersArray) {
    for (i in markersArray) {
      markersArray[i].setMap(null);
    }
    markersArray.length = 0;
  }
}

var markersArray = [];

function addMarker(location) {
  marker = new google.maps.Marker({
    position: location,
    map: map
  });
  markersArray.push(marker);
}

function addAddressToMap(results, status) {
    //map.clearOverlays();
    if (status != google.maps.GeocoderStatus.OK) {
        alert("Kunde inte hitta platsen på kartan!");
        mapError = true;
        //logMapSearch(response.name, "Finns inte");
    } else {
        mapError = false;
		deleteOverlays();
        place = results[0];
        point = place.geometry.location;
        map.setCenter(point);
		map.setZoom(13);
        addMarker(results[0].geometry.location);
		
		var infoBubble = new google.maps.InfoWindow({ content: '<br/>' + place.formatted_address });
		infoBubble.open(map, marker);

        $('#answer7').val(place.address);
        $('#answer5').val(place.geometry.location.lng() + '_' + place.geometry.location.lat());

		getNumberOfHits();
        /*postalCode = null;
        city = null;

        eachRec(place);

        if (postalCode != null) {
            $('#answer5').val(
                postalCode + "_" +
                place.Point.coordinates[0] + "_" +
                place.Point.coordinates[1]);
            logMapSearch(response.name, "OK, hittar postnummer");
        } else if (city != null) {
            $('#answer5').val(
                city + "_" +
                place.Point.coordinates[0] + "_" +
                place.Point.coordinates[1]);
            logMapSearch(response.name, "OK, hittar ort");
        } else {
            $('#answer5').val("");
            map.clearOverlays();
            alert("Din kartsökning är för generell. Ange en mer exakt plats.");
            mapError = true;
            logMapSearch(response.name, "Borde vara OK men hittar varken ort eller postnummer");
        }

        if ($('#answer5').val() != "") {
            $('#guide-map-popup').fadeOut();
        }*/
    }

    /*if (mapError != null && mapError == true)
        setGuideQuestionNumber5Inactive();
    else
        setGuideQuestionNumber5Active();*/
}

function addressChange(event) {
    var path = event.path;

    if (firstLoad && (path == '' || path == '/')) {
        firstLoad = false;
        return;
    }

    if (firstLoad == false && (path == '' || path == '/')) {
        loadForm();
        return;
    }

    if (path.indexOf("resultat") != -1) {
        firstLoad = false;
        if (resultButton) {
            resultButton = false;
            doSubmit();
        }
        else
            LoadResults();
    }
}

function setUpAddressChange() {
    //$.address.change(addressChange);
    //$('#submitlink').address();
	//$('#submitlink').click(doSubmit);
}

var firstLoad;
var resultButton;

$(document).ready(function () {
    firstLoad = true;
	initialize();
	//setUpAddressChange();
});

function updateAddressFieldForMethod2(event) {
    var a = jQuery.trim($('#guide-map-search-address-street').val());
    var b = jQuery.trim($('#school_municipality_id option:selected').text());

    var res = a == '' && b == '' ? '' : a == '' ? b : b == '' ? a : a + ', ' + b;

    $('#guide-map-search-address').val(res);
}

function handleAddress(adr){
	
	console.log("handleAddress");
	console.log(adr);
	var kommun = adr.split(",");
	var select = "";
	
	if(kommun[1].indexOf("County") != -1){ //Firefox bug...
		kommun = kommun[1].split(" ");
		console.log(kommun[1]);
		select = kommun[1];
	}
	else {
		kommun = kommun[1].split(" ");
		console.log(kommun[3]);
		select = kommun[3];
	}
	
	
	$("option:contains('"+select+"')").attr('selected',true);

	$("input#guide-map-search-address-street").val(adr.split(",")[0]);
	updateAddressFieldForMethod2();
	findAddressHidden();
}

function findAddressHidden()
{
	findAddress($('#guide-map-search-address').val(), false);
}

function reverseGeocode(lat, lng){
	console.log("reverseGeocode");
	
	geocoder = new google.maps.Geocoder();
    
	var latlng = new google.maps.LatLng(lat, lng);
    geocoder.geocode({'latLng': latlng}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
        if (results[0]) {
		  console.log(results[1]);
		  console.log(results[0].formatted_address);
		  handleAddress(results[0].formatted_address);
		
        }
      } else {
        alert("Geocoder failed due to: " + status);
      }
    });
    
}

function handleNoGeolocation(msg) {
  console.log("fail");
  console.log(msg);
}

$(document).ready(function(){
	
	console.log("doc ready");
	
	$('.button').button();

	$( "#slider" ).slider({
				range: "min",
				min: 1,
				max: 100,
				value: $( "#school_distance" ).val(),
				slide: function( event, ui ) {
					$( "#school_distance" ).val( ui.value );
				}
	});
	
	$( "#school_distance" ).val( $( "#slider" ).slider( "value" ) );
	
	// try W3C geolocation (preferred)
	if(navigator.geolocation) {
		console.log("go geoloc");
		browserSupportFlag = true;
		navigator.geolocation.getCurrentPosition(function(position) {
			console.log(position.coords.latitude);
		    reverseGeocode(position.coords.latitude,position.coords.longitude)
			//map.setCenter(initialLocation);
		}, function() {
			handleNoGeolocation(browserSupportFlag);
		});
	}  
	else {
		console.log("no geoloc");
		browserSupportFlag = false;
		handleNoGeolocation(browserSupportFlag);
	}
	
	
});