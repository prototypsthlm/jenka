Beskrivning
===========
Här återfinns källkoden för en prototyp som ligger på http://jenka.herokuapp.com.

I det här projektet har vi skapat en liten applikation som kan hämta data från svenska skolverket. Data visas via ett webgränsnitt för användaren som kan göra egna värderingar och filtreringar för att underlätta i en skolvalssituation.

Beroenden
=========
* [Padrino](http://www.padrinorb.com/)

Installation
============
*VARNING*: Datat som medföljer i projektet kan vara gammalt och isåfall behöver ny data hämtas.

För att komma igång behöver man installera Padrino hämta ner koden härifrån, fixa till db/seeds.rb,  samt köra följande tasks:

* padrino rake ar:migrate seed
* rake dbimport:schoolimport
* rake dbimport:personalinfoimport
* rake dbimport:res9import

Samt för geokodning (vilket kan bebhöva göras i etapper beroende på vilket api för geokodning man använder):

* rake geo:do_geocode

Sen kommer man igång med

* padrino start

Licens
======
<div>
<a rel="license" href="http://creativecommons.org/licenses/by/3.0/"><img alt="Creative Commons-licens" style="border-width:0" src="http://i.creativecommons.org/l/by/3.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" href="http://purl.org/dc/dcmitype/InteractiveResource" property="dct:title" rel="dct:type">Jenka</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="http://www.sanocore.se" property="cc:attributionName" rel="cc:attributionURL">Sanocore</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/3.0/">Creative Commons Erkännande 3.0 Unported License</a>.<br />Based on a work at <a xmlns:dct="http://purl.org/dc/terms/" href="http://www.skolverket.se" rel="dct:source">www.skolverket.se</a>.
</div>
