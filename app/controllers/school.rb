Jenka.controllers :school do
  
  # get :index, :map => "/foo/bar" do
  #   session[:foo] = "bar"
  #   render 'index'
  # end

  # get :sample, :map => "/sample/url", :provides => [:any, :js] do
  #   case content_type
  #     when :js then ...
  #     else ...
  # end

  # get :foo, :with => :id do
  #   "Maps to url '/foo/#{params[:id]}'"
  # end

  # get "/example" do
  #   "Hello world!"
  # end

  get :index, :map => '/' do
    # puts "LOCATION MESS"
    # puts @env['REMOTE_ADDR']
    
    # ipaddr = request.location
    # puts ipaddr.city
    
    @address = "" #ipaddr.city == "" ? "" : ipaddr.address
    
    erb :'school/index'
  end

  # get :list do
  #  @selectedSchools = School.all
  #  #render 'school/index'
  #  erb :'school/index'
  # end

  # get :search do
  #  puts "LOCATION MESS"
  #  puts @env['REMOTE_ADDR']
  #  
  #  ipaddr = request.location
  #  puts ipaddr.city
  #  
  #  @adress = ipaddr.city == "" ? "" : ipaddr.address + ", " + ipaddr.city
  #  
  #  erb :'school/search'
  # end
  
  post :getNumberOfHits do
    if params[:answer5] == ""
      coords = 61.6965396, 14.8684927
    else
      coords = params[:answer5].split("_").reverse
      coords = coords[0].to_f, coords[1].to_f
    end
    
    schoolSelection = get_schools(coords, params[:answer6], params[:schoolsizemin], params[:schoolsizemax], params[:school][:municipality_id])
    
    @numberOfResults = schoolSelection.to_a.count
    render_partial 'school/numberOfResults'
  end
  
  post :search do
    
    #if @env['REMOTE_ADDR'] != "127.0.0.1"
      #@adress = @env['REMOTE_ADDR']
    #end

    #School.where("results_ninth_grade > ?", 200).length
    #School.where("results_ninth_grade < ?", 0).length
    #School.find(:all, :conditions => ["number_of_students IS NOT NULL"]).sort_by { |s| s.number_of_students }
    #School.find(:all, :conditions => ["results_ninth_grade IS NOT NULL"]).sort_by { |s| s.results_ninth_grade }
    
    #lngLat = params[:answer5].split("_")
    coords = params[:answer5].split("_").reverse
    #puts "LATLONG: "
    #puts coords[0]
    #puts coords[1]
    coords = coords[0].to_f, coords[1].to_f
    
    #@adress = "83.250.204.13"
    #@distance = params[:answer6].to_f / 1000 # from meters to miles
    #@distance = @distance == 0 ? 1000 : @distance # if 0 no limit (or rather 1000km limit)
    #puts "DISTANCE POST: "
    #puts @distance
    
    #@sizemin = params[:schoolsizemin].to_i
    #@sizemax = params[:schoolsizemax].to_i
    
    #puts "SCHOOL SIZE POST: "
    #puts @sizemin
    #puts @sizemax
    
    #puts "KOMMUN POST: "
    #puts params[:school][:municipality_id]
    #kommun = Municipality.where(:id => params[:school][:municipality_id]).first
    #@selected = kommun #används typ inte?

    #puts "PARAM PRIO POST: "
    #puts params[:school][:rank_prio]
    prio = params[:school][:rank_prio]
    
    #span number of students: 2 - 1094
    #schoolLargerThan = 0
    #schoolSmallerThan = 2000
    
    #with filter cut
    schoolSelection = get_schools(coords, params[:answer6], params[:schoolsizemin], params[:schoolsizemax], params[:school][:municipality_id])
  
    distances = Array.new

    schoolSelection.each do |s|
        dist = s.distance_from(coords[0],coords[1]) #tar ej array, fel i docs
        distances << dist
        s.distance = dist * 1.609344 # miles to m
    end
    
    maxDistance = distances.max.to_s
    minDistance = distances.min.to_s
    
    ranked = schoolSelection.sort_by { |s| cool_sort(s, prio, coords, minDistance.to_f, maxDistance.to_f) }
    
    #ranked.each do |school|
      
      #r = cool_sort(school, prio, coords, minDistance.to_f, maxDistance.to_f)
      
      #if r > 0
        #PUTS@HEROKU GÅR EJ ATT LITA PÅ, VISA OUTPUTS KOMMER INTE IGENOM
        #puts "NAME: " + school.name
        #if !school.number_of_students.nil?
        #  puts "NoS: " + school.number_of_students.to_s
        #end
        
        #puts "\n"
        #puts "QUALT: " + school.qualified_teachers.to_s
        #puts "TCONC: " + school.teacher_concentration.to_s
        #puts "RES9: " + school.results_ninth_grade.to_s
        #puts "RANK: " + r.to_s
        #puts "KOMMUN: " + school.municipality.name
        #puts "\n"
        #puts "\n"
      #end
      
    #end
    
    @selectedByRanking = ranked.reverse.first(10)
    #unranked query !!!
    #@selectedSchools = School.where(:municipality_id => kommun).near([adrtest[0].to_f,adrtest[1].to_f], 10)
    
    render_partial 'school/result'
  end
end