class School < ActiveRecord::Base
  belongs_to :municipality
  geocoded_by :location
  def location
     [address, postalcode, city, 'Sweden'].compact.join(', ')
  end
  #after_validation :geocode 
end
