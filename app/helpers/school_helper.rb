# Helper methods defined here can be accessed in any controller or view in the application

Jenka.helpers do
  def get_schools(coords, answer6, schoolsizemin, schoolsizemax, municipality_id)
    # Distance param
    @distance = params[:answer6].to_f / 1000 # from meters to km
    @distance = @distance == 0 ? 1000 : @distance # if 0 no limit (or rather 1000km limit)
    
    # Size param
    @sizemin = schoolsizemin.to_i
    @sizemax = schoolsizemax.to_i
    
    # Municipality param
    kommun = Municipality.where(:id => municipality_id).first
    
    # Return school selection
    return School.includes("municipality").where("number_of_students >= ? AND number_of_students <= ?", @sizemin, @sizemax).where(:municipality_id => kommun).near(coords, @distance, :units => :km)
  end
  
  def cool_sort(s, prio, coords, minDist, maxDist) #

    prioFactor = 2
    
    if s.qualified_teachers.nil? || s.teacher_concentration.nil? || s.results_ninth_grade.nil?
      return -1000
    end
    
    if s.qualified_teachers <= 0 || s.teacher_concentration <= 0 || s.results_ninth_grade <= 0
      return -1000
    end
    
    rankConc = 0
    #span lärartäthet => 0 - 400.0
    case s.teacher_concentration #teachers per 100 students
      when 0..5
        rankConc = 20 #very low
      when 5..10
        rankConc = 40 #low
      when 10..20 
        rankConc = 60 #medium
      when 20..40
        rankConc = 80 #large
      when 40..100
        rankConc = 100 #high #spec case? often very small school if this high teacher conc. 
        #multiply by 1+(numberofstudents/50)? before switch statement?
      else
        rankConc = 100
    end
    
    #procent
    rankQualified = s.qualified_teachers #0-100%      
    
    rankTeachers = (rankQualified + rankConc)/2
    rankTeachers = (prio.to_i == 3) ? (rankTeachers * prioFactor) : rankTeachers
    
    rankBetyg = (s.results_ninth_grade/320)*100 #0-100%
    rankBetyg = (prio.to_i == 2) ? (rankBetyg * prioFactor) : rankBetyg

    rankDistance = ((maxDist - s.distance_from(coords[0],coords[1]))/(maxDist-minDist))*100
    rankDistance = (prio.to_i == 1) ? (rankDistance * prioFactor) : rankDistance
    
    ranking = (rankTeachers + rankBetyg + rankDistance)/3
    
    return ranking
    
  end
end