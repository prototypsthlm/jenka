# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 10) do

  create_table "accounts", :force => true do |t|
    t.string "name"
    t.string "surname"
    t.string "email"
    t.string "crypted_password"
    t.string "role"
  end

  create_table "municipalities", :force => true do |t|
    t.string "name"
  end

  create_table "schools", :force => true do |t|
    t.string  "name"
    t.string  "address"
    t.string  "postalcode"
    t.string  "city"
    t.string  "phone"
    t.string  "schooltype"
    t.string  "county"
    t.string  "organization_type"
    t.integer "municipality_id"
    t.string  "url",                        :limit => 5000
    t.float   "latitude"
    t.float   "longitude"
    t.float   "distance"
    t.float   "qualified_teachers"
    t.float   "qualified_special_teachers"
    t.float   "teacher_concentration"
    t.integer "number_of_students"
    t.float   "results_ninth_grade"
  end

end
