class FixColumnName < ActiveRecord::Migration
  def self.up
    rename_column :schools, :type, :schooltype
  end

  def self.down
    rename_column :schools, :schooltype, :type
  end
end
