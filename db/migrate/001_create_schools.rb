class CreateSchools < ActiveRecord::Migration
  def self.up
    create_table :schools do |t|
      t.string :name
      t.string :address
      t.string :postalcode
      t.string :city
      t.string :phone
      t.string :type
      t.string :county
      t.string :municipality
      t.string :organization_type
    end
  end

  def self.down
    drop_table :schools
  end
end
