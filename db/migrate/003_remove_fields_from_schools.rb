class RemoveFieldsFromSchools < ActiveRecord::Migration
  def self.up
    change_table :schools do |t|
      t.remove :municipality
    end
  end

  def self.down
    change_table :schools do |t|
      t.string :municipality
    end
  end
end
