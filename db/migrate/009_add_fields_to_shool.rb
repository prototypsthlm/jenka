class AddFieldsToShool < ActiveRecord::Migration
  def self.up
    change_table :schools do |t|
      t.float :qualified_teachers
      t.float :qualified_special_teachers
      t.float :teacher_concentration
      t.integer :number_of_students
    end
  end

  def self.down
    change_table :schools do |t|
      t.remove :qualified_teachers
      t.remove :qualified_special_teachers
      t.remove :teacher_concentration
      t.remove :number_of_students
    end
  end
end
