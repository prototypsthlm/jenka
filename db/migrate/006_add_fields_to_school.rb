class AddFieldsToSchool < ActiveRecord::Migration
  def self.up
    change_table :schools do |t|
      t.string :url, :limit => 5000
    end
  end

  def self.down
    change_table :schools do |t|
      t.remove :url
    end
  end
end
