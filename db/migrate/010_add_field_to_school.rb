class AddFieldToSchool < ActiveRecord::Migration
  def self.up
    change_table :schools do |t|
      t.float :results_ninth_grade
    end
  end

  def self.down
    change_table :schools do |t|
      t.remove :results_ninth_grade
    end
  end
end
