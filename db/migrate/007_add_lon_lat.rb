class AddLonLat < ActiveRecord::Migration
  def self.up
    change_table :schools do |t|
      t.float :latitude
      t.float :longitude
      t.float :distance
    end
  end

  def self.down
    change_table :schools do |t|
      t.remove :latitude
      t.remove :longitude
      t.remove :distance
    end
  end
end
