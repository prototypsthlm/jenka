Admin.controllers :schools do

  get :index do
    @schools = School.all
    render 'schools/index'
  end

  get :new do
    @school = School.new
    render 'schools/new'
  end

  post :create do
    @school = School.new(params[:school])
    if @school.save
      flash[:notice] = 'School was successfully created.'
      redirect url(:schools, :edit, :id => @school.id)
    else
      render 'schools/new'
    end
  end

  get :edit, :with => :id do
    @school = School.find(params[:id])
    render 'schools/edit'
  end

  put :update, :with => :id do
    @school = School.find(params[:id])
    if @school.update_attributes(params[:school])
      flash[:notice] = 'School was successfully updated.'
      redirect url(:schools, :edit, :id => @school.id)
    else
      render 'schools/edit'
    end
  end

  delete :destroy, :with => :id do
    school = School.find(params[:id])
    if school.destroy
      flash[:notice] = 'School was successfully destroyed.'
    else
      flash[:error] = 'Impossible destroy School!'
    end
    redirect url(:schools, :index)
  end
end