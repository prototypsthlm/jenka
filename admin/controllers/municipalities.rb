Admin.controllers :municipalities do

  get :index do
    @municipalities = Municipality.all
    render 'municipalities/index'
  end

  get :new do
    @municipality = Municipality.new
    render 'municipalities/new'
  end

  post :create do
    @municipality = Municipality.new(params[:municipality])
    if @municipality.save
      flash[:notice] = 'Municipality was successfully created.'
      redirect url(:municipalities, :edit, :id => @municipality.id)
    else
      render 'municipalities/new'
    end
  end

  get :edit, :with => :id do
    @municipality = Municipality.find(params[:id])
    render 'municipalities/edit'
  end

  put :update, :with => :id do
    @municipality = Municipality.find(params[:id])
    if @municipality.update_attributes(params[:municipality])
      flash[:notice] = 'Municipality was successfully updated.'
      redirect url(:municipalities, :edit, :id => @municipality.id)
    else
      render 'municipalities/edit'
    end
  end

  delete :destroy, :with => :id do
    municipality = Municipality.find(params[:id])
    if municipality.destroy
      flash[:notice] = 'Municipality was successfully destroyed.'
    else
      flash[:error] = 'Impossible destroy Municipality!'
    end
    redirect url(:municipalities, :index)
  end
end